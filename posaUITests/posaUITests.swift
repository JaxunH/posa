//
//  posaUITests.swift
//  posaUITests
//
//  Created by Jaxun on 2018/6/4.
//  Copyright © 2018年 STARLUX. All rights reserved.
//

import XCTest

class PosaUITests: XCTestCase {

  override func setUp() {
    super.setUp()

    // Put setup code here. This method is called before the invocation of each test method in the class.

    // In UI tests it is usually best to stop immediately when a failure occurs.
    continueAfterFailure = false
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    XCUIApplication().launch()

    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
  }

  override func tearDown() {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    super.tearDown()
  }

  func testExample() {
    
    
    
    let app = XCUIApplication()
    app.buttons["打卡系統"].tap()
    
    let button = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .button).element(boundBy: 2)
    button.tap()
    button.tap()
    app/*@START_MENU_TOKEN@*/.buttons["CalenderButton"]/*[[".buttons[\"2018-06-08 Fri\"]",".buttons[\"CalenderButton\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
    app.buttons["上班：08:19 下班 : 17:47 "].tap()
    app.staticTexts["明細 >"].tap()
    
    
    
  }

}
