// Created by Jaxun on 2018/5/22.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation


class PunchTimeDto: JsonDto {
  public let punchedServerTime:String

  required init(_ punchedServerTime:String) {
    self.punchedServerTime = punchedServerTime;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["sys_time"] as! String)
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:PunchTimeDto, rhs:PunchTimeDto) -> Bool {
    fatalError("== has not been implemented")
  }
}