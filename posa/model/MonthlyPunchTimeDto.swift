//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class MonthlyPunchTimeDto: JsonDto {
  public let fullDate:String
  public let date:String
  public let recordList:Array<String>

  required init(
      _ fullDate:String,
      _ date:String,
      _ recordList:Array<String>) {
    self.fullDate  = fullDate;
    self.date = date;
    self.recordList = recordList
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(
        raw["fullDate"] as! String,
        raw["date"] as! String,
        raw["recordList"] as! Array<String>)
  }


  private(set) var hashValue:Int = 0

  static func ==(lhs:MonthlyPunchTimeDto, rhs:MonthlyPunchTimeDto) -> Bool {
    fatalError("== has not been implemented")
  }
}
