//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class BaseAbnormalDto: JsonDto {
  public let date:String
  public let status:[Any?]?

  required init(
      _ date:String,
      _ status:[Any?]?) {
    self.date  = date;
    self.status = status
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {

    return self.init(
        raw["date"] as! String,
        raw["status"] as? [Any?])
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:BaseAbnormalDto, rhs:BaseAbnormalDto) -> Bool {
    fatalError("== has not been implemented")
  }
}
