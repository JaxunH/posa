//
// Created by Jaxun on 2018/5/22.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation


class BssidListDto: JsonDto {
  public let bssidList:[String]

  required init(_ bssidList:[String]) {
    self.bssidList = bssidList;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["ssid"] as! [String])
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:BssidListDto, rhs:BssidListDto) -> Bool {
    fatalError("== has not been implemented")
  }
}