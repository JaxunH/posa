//
// Created by Jaxun on 2018/5/22.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation


class RecordsDto: JsonDto {
  public let workOnRecordList:[String]
  public let workOffRecordList:[String]

  required init(
      _ workOnRecordList:[String],
      _ workOffRecordList:[String]
  ) {
    self.workOnRecordList = workOnRecordList;
    self.workOffRecordList = workOffRecordList;
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["workOnRecordList"] as! [String],
        raw["workOffRecordList"] as! [String]
    )
  }

  private(set) var hashValue:Int = 0

  static func ==(lhs:RecordsDto, rhs:RecordsDto) -> Bool {
    fatalError("== has not been implemented")
  }
}