//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class PagerDto: JsonDto {
  public let embedded:[String: Any?]?

  required init(_ embedded:[String: Any?]?) {
    self.embedded = embedded
  }

  class func fromDictionary(_ raw:[String: Any?]) -> Self {
    return self.init(raw["_embedded"] as? [String: Any?])
  }

  var hashValue:Int {
    return embedded?.description.hashValue ?? 0
  }

  static func ==(lhs:PagerDto, rhs:PagerDto) -> Bool {
    if let le = lhs.embedded,
       let re = rhs.embedded {
      return NSDictionary(dictionary: le).isEqual(to: re);
    }

    return (lhs.embedded == nil) && (rhs.embedded == nil)
  }

  func resolveEmbeddedArray(_ name:String) -> Array<[String: Any?]> {
    if let list = embedded?[name] {
      return (list ?? []) as! Array<[String: Any?]>
    }
    return []
  }
}
