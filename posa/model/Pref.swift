//
// Created by jaxun on 12/28/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation

class Pref {

  var ottToken:String? {
    get {
      return UserDefaults.standard.value(forKey: "ottToken") as! String?
    }
    set(token) {
      UserDefaults.standard.setValue(token, forKey: "ottToken")
    }
  }

  var isRegistered:Bool {
    return ottToken != nil
  }

  var setOnWork:String? { //todo: rename
    get {
      return UserDefaults.standard.value(forKey: "isOnWork") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "isOnWork")
    }
  }

  var isOnWorkPunched:Bool {
    return setOnWork != nil
  }

  var aliasPhoto:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasPhoto") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasPhoto")
    }
  }

  var aliasAccount:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasAccount") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasAccount")
    }
  }
  var aliasName:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasName") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasName")
    }
  }

  var aliasNameEn:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasNameEn") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasNameEn")
    }
  }

  var aliasNum:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasNum") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasNum")
    }
  }

  var aliasJobTitle:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasJobTitle") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasJobTitle")
    }
  }

  var aliasTel:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasTel") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasTel")
    }
  }

  var aliasSex:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasSex") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasSex")
    }
  }

  var aliasSiteFloor:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasSiteFloor") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasSiteFloor")
    }
  }

  var aliasDepartment:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasDepartment") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasDepartment")
    }
  }

  var aliasDepartmentEn:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasDepartmentEn") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasDepartmentEn")
    }
  }

  var aliasOnBoardDay:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasOnBoardDay") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasOnBoardDay")
    }
  }

  var aliasUniqueId:String? {
    get {
      return UserDefaults.standard.value(forKey: "aliasUniqueId") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "aliasUniqueId")
    }
  }

  var fcmPushToken:String? {
    get {
      return UserDefaults.standard.value(forKey: "pushToken") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "pushToken")
    }
  }

  var base64AccountId:String? { // with base64 encode used for BPM
    get {
      return UserDefaults.standard.value(forKey: "base64AccountId") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "base64AccountId")
    }
  }

  var base64BPMTel:String? { // with base64 encode used for BPM
    get {
      return UserDefaults.standard.value(forKey: "base64BPMTel") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "base64BPMTel")
    }
  }

  var uniqueBPMCode:String? { // with base64 encode used for BPM
    get {
      return UserDefaults.standard.value(forKey: "uniqueBPMCode") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "uniqueBPMCode")
    }
  }

  var uniqueBPMCode2:String? { // with base64 encode used for BPM
    get {
      return UserDefaults.standard.value(forKey: "uniqueBPMCode2") as! String?
    }
    set(token) {
      UserDefaults.standard.set(token, forKey: "uniqueBPMCode2")
    }
  }

}
