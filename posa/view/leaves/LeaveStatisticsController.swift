import UIKit
import SnapKit
import Savory
import Alertift
import PKHUD


class LeaveStatisticsController: UIViewController {

  fileprivate let pref = Pref()
  fileprivate let leaveStatisticsService:LeaveStatisticsService = Injector.inject()
  fileprivate var accountId = String()
  fileprivate var sex = String()
  fileprivate var choseYearForMenstrualLeave = String()
  fileprivate var choseYearString = String()
  private let yearTitle = UIButton(type: .system)

  fileprivate var leaveAllResult:[EmployeeAllLeaveDto] = []

  fileprivate let expandableView = CollapsibleTableSectionViewController()

  fileprivate let menstrualLeaveView = MenstrualLeaveView(frame: .zero)

  fileprivate let logoImg = UIImageView(image: UIImage(named: "title_logo"))

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  override func viewWillAppear(_ animated:Bool) {
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    navigationController?.navigationBar.backIndicatorImage = UIImage(named: "icon_back")
    navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "icon_back")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    // check F / M
    if let sex = pref.aliasSex {
        self.sex = sex
    }
    
    setNaviLogo()

    view.backgroundColor = Themes.starluxGold

    expandableView.delegate = self

    yearTitle.addTarget(self, action: #selector(clickChoseYear), for: .touchUpInside)

    let currentYear = Utils.getYear(Date())
    yearTitle.setTitle("\(currentYear) \(I18N.key("attendance_record"))" + " ", for: .normal)
    yearTitle.setTitleColor(.white, for: .normal)
    yearTitle.titleLabel?.font = Themes.font20

    let arrowIcon = UIImageView(image: UIImage(named: "right_arrow"))
    arrowIcon.contentMode = .scaleAspectFit

    menstrualLeaveView.handleController = self.navigationController!

    self.view.addSubview(yearTitle)
    self.view.addSubview(arrowIcon)
    self.view.addSubview(expandableView.view)
    self.view.addSubview(menstrualLeaveView)

    fetchApi(choseYear: "\(currentYear)")

    logoImg.snp.makeConstraints { make in
      make.size.equalTo(CGSize(width: Screens.width / 3, height: 30))
    }

    yearTitle.snp.makeConstraints { maker in
      maker.top.equalTo(15)
      //maker.centerX.equalToSuperview()
      maker.leading.equalTo(25)
      maker.size.equalTo(CGSize(width: 230, height: 30))
    }

    arrowIcon.snp.makeConstraints { maker in
      maker.size.equalTo(CGSize(width: 24, height: 24))
      maker.leading.equalTo(yearTitle.snp.trailing)
      maker.centerY.equalTo(yearTitle)
    }

    expandableView.view.snp.makeConstraints { maker in
      maker.top.equalTo(menstrualLeaveView.snp.bottom)
      maker.leading.trailing.bottom.equalToSuperview()
    }

    menstrualLeaveView.snp.makeConstraints { maker in
      maker.top.equalTo(yearTitle.snp.bottom).offset(15)
      maker.leading.equalTo(0)
      maker.size.equalTo(CGSize(width: Screens.width, height: 50))
      if self.sex == "M" {
        menstrualLeaveView.alpha = 0
        maker.size.equalTo(CGSize(width: Screens.width, height: 1))
      }
    }
  }

  private func setNaviLogo() {
    let choseMode: UIViewContentMode = Dimension.isPad ? .scaleAspectFit : .scaleAspectFill
    logoImg.contentMode = choseMode
    navigationItem.titleView = logoImg
  }

  func clickChoseYear() {
    var currentYear:String = Utils.getYear(Date())
//    currentYear = "2050" //for test multi years
    var displayYears:[String] = [currentYear]
    var standardYear = Int(currentYear)
    if let onBoard = pref.aliasOnBoardDay {
      choseYearString = String(onBoard.suffix(4)) // ex: "day_onboard": "Apr 02 2018"
    }
    var onBoardYear = Int(choseYearString)

    while standardYear! > onBoardYear! {
      print(standardYear!)
      standardYear = standardYear! - 1
      displayYears.append("\(standardYear!)")
    }

    Alertift.actionSheet(message: nil)
        .actions(displayYears) //["2018","2017"...]
        .action(.cancel("cancel"))
        .finally { action, index in
          if action.style == .cancel {
            return
          }
          self.fetchApi(choseYear: displayYears[index])
          self.menstrualLeaveView.choseYear = displayYears[index]
          self.yearTitle.setTitle("\(displayYears[index]) \(I18N.key("attendance_record"))" + " ", for: .normal)
//          print(displayYears[index])
        }
        .show()
  }

  private func fetchApi(choseYear:String) {

    leaveAllResult.removeAll()

    if let aId = pref.aliasAccount {
      accountId = aId
    }

    HUD.show(.progress)

    leaveStatisticsService.fetchAllLeave(accountId: accountId, year: choseYear)
        .subscribe(onNext: { [weak self] result -> Void in
          guard let strongSelf = self else {
            return
          }
          let recordsDto = result as! BaseArrayDto

          if let employeeList = recordsDto.data {
            employeeList.enumerated().forEach { _, dto in
              let rawDto = dto as! [String: Any?]
              let employeeAllLeaveDto = EmployeeAllLeaveDto.fromDictionary(rawDto)
              //print("------\(employeeAllLeaveDto.leaveName)")
              strongSelf.leaveAllResult.append(employeeAllLeaveDto)
              strongSelf.expandableView._tableView.reloadData()

              HUD.flash(.progress)
            }
          }
        }).addDisposableTo(self.disposeBag)
  }
}

extension LeaveStatisticsController: CollapsibleTableSectionDelegate {
  public func numberOfSections(_ tableView:UITableView) -> Int {
    return leaveAllResult.count
  }

  public func collapsibleTableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
    let cell:LeaveStatisticsBodyCell = tableView.dequeueReusableCell(withIdentifier: NSStringFromClass(
        LeaveStatisticsBodyCell.self)) as? LeaveStatisticsBodyCell ??
        LeaveStatisticsBodyCell(style: .default, reuseIdentifier: NSStringFromClass(LeaveStatisticsBodyCell.self))

    if leaveAllResult.count > 0 && indexPath.section < leaveAllResult.count {
      let leave = leaveAllResult[indexPath.section] as EmployeeAllLeaveDto
      cell.bind(leave)
      cell.backgroundColor = Themes.starluxLight
    }

    cell.selectionStyle = .none
    return cell
  }

  func collapsibleTableView(_ tableView:UITableView, titleForHeaderInSection section:Int) -> String? {
    return leaveAllResult[section].leaveNameEn
  }

  func collapsibleTableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
    return 1
  }


  public func collapsibleTableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
    return 50
  }

  public func shouldCollapseByDefault(_ tableView:UITableView) -> Bool {
    return true
  }

//  public func collapsibleTableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath) {
//  }
}

