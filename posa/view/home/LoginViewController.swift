//
// Created by STARLUX on 1/6/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import Spring
import PKHUD
import Alertift
import SwiftKeychainWrapper

class LoginViewController: UIViewController, UITextFieldDelegate {

  fileprivate let pref = Pref()
  fileprivate let loginService:LoginService = Injector.inject()

  fileprivate let emailLabel = UILabel(frame: .zero)
  fileprivate let email = UITextField(frame: .zero)
  fileprivate var isEmailKeyIn = false
  fileprivate let passwordLabel = UILabel(frame: .zero)
  fileprivate let password = UITextField(frame: .zero)
  fileprivate let loginBtn = UIButton(type: .system)

  fileprivate let contentView = UIView(frame: .zero)

  fileprivate var isLogin = false
  fileprivate var punchType = 9 // punch on = 0, punch off = 1

  override func viewDidAppear(_ animated:Bool) {
    super.viewDidAppear(animated)
  }
  func dismissKeyboard() {
    view.endEditing(true)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    NotificationCenter.default.addObserver(self,
        selector: #selector(keyboardWillShow),
        name: NSNotification.Name.UIKeyboardWillShow,
        object: nil)
    NotificationCenter.default.addObserver(self,
        selector: #selector(keyboardWillHide),
        name: NSNotification.Name.UIKeyboardWillHide,
        object: nil)

    let logoImg = UIImageView(image: UIImage(named: "starlux_logo"))
    logoImg.contentMode = .scaleAspectFit
    self.view.addSubview(logoImg)

    self.view.addSubview(contentView)

    let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
    contentView.addGestureRecognizer(tap)

    email.placeholder = "E-mail"
    email.isSecureTextEntry = false
    email.borderStyle = .none
    email.returnKeyType = .done
    email.textColor = .gray
    email.delegate = self
    email.autocorrectionType = .no
    contentView.addSubview(email)

    let underLine = UIView(frame: .zero)
    underLine.backgroundColor = .lightGray
    contentView.addSubview(underLine)


    password.placeholder = "Password"
    password.isSecureTextEntry = true
    password.borderStyle = .none
    password.returnKeyType = .done
    password.textColor = .gray
    password.delegate = self
    password.autocorrectionType = .no
    contentView.addSubview(password)

    let underLine2 = UIView(frame: .zero)
    underLine2.backgroundColor = .gray
    contentView.addSubview(underLine2)

    let loginBtn = UIButton(type: .system)
    loginBtn.backgroundColor = Themes.starluxGold
    loginBtn.titleLabel?.font = Themes.font20
    loginBtn.setTitle("Login", for: UIControlState.normal) // todo:i18n
    loginBtn.setTitleColor(.white, for: UIControlState.normal)
    loginBtn.addTarget(self, action: #selector(clickLogin), for: UIControlEvents.touchUpInside)
    contentView.addSubview(loginBtn)

    logoImg.snp.makeConstraints({ (make) -> Void in
      make.top.equalTo(120)
      make.centerX.equalToSuperview()
      make.size.equalTo(CGSize(width: 250, height: 80))
    })

    contentView.snp.makeConstraints { maker in
      maker.edges.equalTo(view)
    }

    email.snp.makeConstraints({ (make) -> Void in
      make.leading.equalTo(50)
      make.trailing.equalTo(-50)
      make.height.equalTo(50)
      make.bottom.equalTo(underLine)
    })
    underLine.snp.makeConstraints({ (make) -> Void in
      make.leading.trailing.bottom.equalTo(email)
      make.height.equalTo(1)
      make.bottom.equalTo(password.snp.top).offset(-30)
    })
    password.snp.makeConstraints({ (make) -> Void in
      make.leading.equalTo(50)
      make.trailing.equalTo(-50)
      make.height.equalTo(50)
      make.bottom.equalTo(underLine2)
    })
    underLine2.snp.makeConstraints({ (make) -> Void in
      make.leading.trailing.bottom.equalTo(password)
      make.height.equalTo(1)

      make.bottom.equalTo(loginBtn.snp.top).offset(-50)
    })
    loginBtn.snp.makeConstraints({ (make) -> Void in
      make.leading.trailing.equalTo(contentView)
      make.height.equalTo(100)
      make.bottom.equalTo(-60)
    })

    if (isLogin) {
      hideNavigationBar()
      let controller = MainPageEntry(isLogin: true, type: punchType)
      controller.view.backgroundColor = UIColor.white
      navigationController?.pushViewController(controller, animated: false)
    }
  }

  override func viewWillAppear(_ animated:Bool) {
    navigationController?.isNavigationBarHidden = true
    navigationController?.interactivePopGestureRecognizer?.isEnabled = false
  }

  override func viewWillDisappear(_ animated:Bool) {
    hideNavigationBar()
  }

  private func hideNavigationBar() {
    navigationController?.isNavigationBarHidden = false
    navigationController?.navigationBar.tintColor = Themes.starluxGold
    navigationController?.navigationBar.isTranslucent = false

    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    navigationController?.navigationBar.backIndicatorImage = UIImage(named: "icon_logout")
    navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "icon_logout")
  }

  func clickLogin() {
    HUD.show(.progress)
    loginService.fetchAlias(account: email.text!, password: password.text!)
        .subscribe(onNext: { [weak self] result -> Void in
          //print("accountFullDto >> \(result)")
          guard let strongSelf = self else {
            return
          }
          let dto = result as! BaseDto
          //print(baseDto)

          if dto.code == 1 {
            HUD.flash(.progress)

            strongSelf.accountSetting(dto: dto)

          } else {
            let errorMsg = dto.msg as! String
            HUD.flash(.labeledError(title: errorMsg, subtitle: nil), delay: 1.0)
            /* Alertift.alert(title: errorMsg, message: nil)
                .action(.default("OK"))
                .show(on: self) */
          }
        }).addDisposableTo(self.disposeBag)
  }

  fileprivate func accountSetting(dto:BaseDto) {
    let accountFullDto = AccountFullDto.fromDictionary(dto.data!)
    pref.aliasAccount = accountFullDto.empNo
    pref.aliasNum = accountFullDto.empNo
    pref.aliasName = accountFullDto.name
    pref.aliasJobTitle = accountFullDto.jobEnTitle
    pref.aliasDepartment = accountFullDto.departmentName
    pref.aliasTel = accountFullDto.tel
    pref.aliasSex = accountFullDto.sex
    pref.aliasSiteFloor = accountFullDto.floorSite
    pref.aliasPhoto = accountFullDto.aliasIconUrl
    pref.aliasOnBoardDay = accountFullDto.onBoardDay
    pref.aliasNameEn = accountFullDto.eName
    pref.aliasDepartmentEn = accountFullDto.departmentEName
    pref.aliasUniqueId = UIDevice.current.identifierForVendor?.uuidString // also save to keychain

    // try to save to keychain
    // https://github.com/jrendel/SwiftKeychainWrapper
    KeychainWrapper.standard.set((UIDevice.current.identifierForVendor?.uuidString)!, forKey: "aliasUniqueId")

    print("uuid = \(UIDevice.current.identifierForVendor?.uuidString)")

    directPage()
  }

  fileprivate func directPage() {
    let controller = MainPageEntry(isLogin: true, type: punchType)
    controller.view.backgroundColor = UIColor.white
    navigationController?.pushViewController(controller, animated: true)
  }

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  init(isLogin:Bool) {
    super.init(nibName: nil, bundle: nil)
    self.isLogin = isLogin
  }

  init(isLogin:Bool, type:Int) {
    super.init(nibName: nil, bundle: nil)
    self.isLogin = isLogin
    self.punchType = type
  }

  func textFieldShouldReturn(_ textField:UITextField) -> Bool {
    view.endEditing(true)
    return false
  }

  func textFieldDidBeginEditing(_ textField:UITextField) {
    print("textFieldDidBeginEditing")
    isEmailKeyIn = false
    if (textField == self.email) {
      isEmailKeyIn = true
      print("textFieldDidBeginEditing == emailTextField")
      email.text = "@starlux-airlines.com"
      setCursorPosition(input: email, position: 0)
    }
  }

  private func setCursorPosition(input:UITextField, position:Int) {
    let position = input.position(from: input.beginningOfDocument, offset: position)!
    input.selectedTextRange = input.textRange(from: position, to: position)
  }

  func keyboardWillShow(notification:Notification) {
    viewPositionModify(show: true)
  }

  func keyboardWillHide(notification:Notification) {
    viewPositionModify(show: false)
  }

  func viewPositionModify (show:Bool) -> () {

    if isEmailKeyIn && !Dimension.isPad {
      return
    }

    if show {
      let keyboardSize = Dimension.isPhoneX ? 233 : Screens.height / 3 - 80
    contentView.snp.remakeConstraints { maker in
      maker.leading.trailing.top.equalTo(view)
      maker.bottom.equalTo(-keyboardSize)
     }
    } else {
      contentView.snp.remakeConstraints { maker in
        maker.edges.equalTo(view)
      }
    }
  }
}
