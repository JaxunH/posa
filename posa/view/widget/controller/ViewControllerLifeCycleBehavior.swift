//
// Created by jaxun on 1/5/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import ObjectiveC

protocol ViewControllerLifeCycleBehavior {
  func viewDidLoad(_ parentController:UIViewController)

  func viewWillAppear(_ parentController:UIViewController)

  func viewDidAppear(_ parentController:UIViewController)

  func viewWillDisappear(_ parentController:UIViewController)

  func viewDidDisappear(_ parentController:UIViewController)

  func viewWillLayoutSubviews(_ parentController:UIViewController)

  func viewDidLayoutSubviews(_ parentController:UIViewController)
}

//extension ViewControllerLifeCycleBehavior {
//  func viewDidLoad(_ parentController:UIViewController) {
//  }
//
//  func viewWillAppear(_ parentController:UIViewController) {
//  }
//
//  func viewDidAppear(_ parentController:UIViewController) {
//  }
//
//  func viewWillDisappear(_ parentController:UIViewController) {
//  }
//
//  func viewDidDisappear(_ parentController:UIViewController) {
//  }
//
//  func viewWillLayoutSubviews(_ parentController:UIViewController) {
//  }
//
//  func viewDidLayoutSubviews(_ parentController:UIViewController) {
//  }
//}

private class LifeCycleBehaviorViewController: UIViewController {
  private var behaviors:[ViewControllerLifeCycleBehavior]

  required init?(coder aDecoder:NSCoder) {
    self.behaviors = []
    super.init(coder: aDecoder)
  }

  required init(_ behaviors:[ViewControllerLifeCycleBehavior]) {
    self.behaviors = Array(behaviors)
    super.init(nibName: nil, bundle: nil)
  }

  func addNewBehaviors(_ newBehaviors:[ViewControllerLifeCycleBehavior]) {
    behaviors.append(contentsOf: newBehaviors)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.isHidden = true
    applyBehaviors { (behavior, parentVc) in
      behavior.viewDidLoad(parentVc)
    }
  }

  override func viewWillAppear(_ animated:Bool) {
    super.viewWillAppear(animated)
    applyBehaviors { (behavior, parentVc) in
      behavior.viewWillAppear(parentVc)
    }
  }

  override func viewDidAppear(_ animated:Bool) {
    super.viewDidAppear(animated)
    applyBehaviors { (behavior, parentVc) in
      behavior.viewDidAppear(parentVc)
    }
  }

  override func viewWillDisappear(_ animated:Bool) {
    super.viewWillDisappear(animated)
    applyBehaviors { (behavior, parentVc) in
      behavior.viewWillDisappear(parentVc)
    }
  }

  override func viewDidDisappear(_ animated:Bool) {
    super.viewDidDisappear(animated)
    applyBehaviors { (behavior, parentVc) in
      behavior.viewDidDisappear(parentVc)
    }
  }

  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    applyBehaviors { (behavior, parentVc) in
      behavior.viewWillLayoutSubviews(parentVc)
    }
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    applyBehaviors { (behavior, parentVc) in
      behavior.viewDidLayoutSubviews(parentVc)
    }
  }

  private func applyBehaviors(_ function:(ViewControllerLifeCycleBehavior, UIViewController) -> Void) {
    guard let parentVc = self.parent else {
      return;
    }
    self.behaviors.forEach { behavior in
      function(behavior, parentVc)
    }
  }
}

protocol LifeCycleBehavioral {
  func addBehaviors(_ behaviors:[ViewControllerLifeCycleBehavior])
}

extension UIViewController: LifeCycleBehavioral {

  func addBehaviors(_ behaviors:[ViewControllerLifeCycleBehavior]) {
    let addedBehaviors = self.childViewControllers
        .filter {
          $0 is LifeCycleBehaviorViewController
        }.map {
          $0 as! LifeCycleBehaviorViewController
        }

    if addedBehaviors.isEmpty {
      let lifeController = LifeCycleBehaviorViewController(behaviors)
      addChildViewController(lifeController)
      view.addSubview(lifeController.view)
      lifeController.didMove(toParentViewController: self)
    } else {
      addedBehaviors[0].addNewBehaviors(behaviors)
    }
  }
}