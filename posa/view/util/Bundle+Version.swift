//
// Created by STARLUX on 1/12/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation

extension Bundle {

  class var applicationVersionNumber:String {
    if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
      return version
    } else {
      return "x.y.z"
    }
  }
  class var applicationBuildNumber:String {
    if let build = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
      return build
    }
    return "???"
  }

}