//
// Created by JaxunC on 2018/6/29.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import UIKit
import SnapKit

extension UIView {
  public var safeArea:ConstraintBasicAttributesDSL {
    if #available(iOS 11.0, *) {
      return safeAreaLayoutGuide.snp
    }
    return self.snp
  }
}