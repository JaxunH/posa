//
// Created by Jaxun on 2018/5/15.
// Copyright (c) 2018 STARLUX. All rights reserved.
//


import Foundation
import UIKit
import XLPagerTabStrip
import RxSwift
import SnapKit
import ActionSheetPicker_3_0

class PunchTimeLagacyDetailPage: UIViewController {

  fileprivate let workTime = UILabel(frame: .zero)
  fileprivate var workTimeDisplay = UILabel(frame: .zero)
  fileprivate let offTime = UILabel(frame: .zero)
  fileprivate var offTimeDisplay = UILabel(frame: .zero)
  fileprivate let punchTime = UILabel(frame: .zero)
  fileprivate var punchTimeOnDisplay = UILabel(frame: .zero)
  fileprivate var punchTimeOffDisplay = UILabel(frame: .zero)
  fileprivate var mockCurrentDay = Date()

  fileprivate var datePicker = UIDatePicker(frame: .zero)
  fileprivate let displayTimeBtn:UIButton = UIButton(type: .system)

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  // MARK: - UIViewController
  override func viewDidLoad() {
    super.viewDidLoad()

    title = "班別與打卡資料"
    initLayout()
    setLayout()
  }

  private func initLayout() {
    updateCurrentDateApi(NSDate())
    self.displayTimeBtn.backgroundColor = .orange
    self.displayTimeBtn.setTitleColor(.white, for: UIControlState.normal)
    self.displayTimeBtn.addTarget(self, action: #selector(clickChoseDay), for: UIControlEvents.touchUpInside)
    self.view.addSubview(self.displayTimeBtn)

    initPunchTimeDetailLayout()
  }

  private func initPunchTimeDetailLayout() {
    self.workTime.text = I18N.key("attendance")
    self.offTime.text = I18N.key("at_lunch")
//    self.punchTime.text = "打卡時間"
//    self.workTimeDisplay.text = "08:26 ~ 17:26"
//    self.offTimeDisplay.text = "12:30 ~ 13:30\n 17:26 ~ 17:56"
    self.offTimeDisplay.numberOfLines = 2
//    self.punchTimeOnDisplay.text = "上班 08:26"
//    self.punchTimeOffDisplay.text = "下班 17:56"

    self.view.addSubview(self.workTime)
    self.view.addSubview(self.workTimeDisplay)
    self.view.addSubview(self.offTime)
    self.view.addSubview(self.offTimeDisplay)
    self.view.addSubview(self.punchTime)
    self.view.addSubview(self.punchTimeOnDisplay)
    self.view.addSubview(self.punchTimeOffDisplay)
  }

  func clickChoseDay() {
    let datePicker = ActionSheetDatePicker(title: nil,
        datePickerMode: UIDatePickerMode.date,
        selectedDate: Date(),
        doneBlock: { [weak self]
        picker, value, index in

          guard let strongSelf = self else {
            return
          }
          if let choseDay = value {
            print(choseDay)

            let today = choseDay as! Date
            if strongSelf.isSameDay(today, strongSelf.mockCurrentDay) {
              return
              print(">>> isSameDay")
            }
            strongSelf.updateCurrentDateApi(choseDay)
            strongSelf.mockCurrentDay = today
          }

          return
        },
        cancel: { ActionStringCancelBlock in return },
        origin: self.view)

    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd EEE"
    let fromDateTime = formatter.date(from: "2017-09-01 08:00")
    let endDateTime = formatter.date(from: "2020-12-25 19:45")

    if let datePicker = datePicker {
      datePicker.minimumDate = fromDateTime
      datePicker.maximumDate = endDateTime
      datePicker.locale = Locale(identifier: "zh-TW")
      datePicker.show()
    }
  }

  private func isSameDay(_ today:Date, _ day:Date) -> Bool { //todo: mock
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    let todayFormat = formatter.string(from: today)
    let choseDayFormat = formatter.string(from: day)

    if todayFormat == choseDayFormat {
      return true
    } else {
      return false
    }
  }

  private func updateCurrentDateApi(_ value:Any) {
    let newDate = value as! Date
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd EEE"
    let fromDateTime = formatter.string(from: newDate)
    self.displayTimeBtn.setTitle(fromDateTime, for: UIControlState.normal)
    print(fromDateTime)

    updateMockDate()
  }

  private func updateMockDate() {

    let rHour:UInt32 = arc4random_uniform(9) + 8
    let rSecond:UInt32 = arc4random_uniform(59) + 1

    self.workTimeDisplay.text = "\(rHour):\(rSecond) ~ 17:\(rSecond)"
    self.offTimeDisplay.text = "12:30 ~ 13:30\n 17:\(rSecond) ~ 17:\(rSecond + 30)"
    self.punchTimeOnDisplay.text = "上班 \(rHour):\(rSecond)"
    self.punchTimeOffDisplay.text = "下班 17:\(rSecond)"
  }

  private func setLayout() {
    self.displayTimeBtn.snp.makeConstraints { make in
      make.top.equalTo(15)
      make.leading.trailing.equalTo(0)
      make.height.greaterThanOrEqualTo(40)
    }
    self.workTime.snp.makeConstraints { make in
      make.top.equalTo(self.displayTimeBtn.snp.bottom).offset(15)
      make.leading.equalTo(30)
    }
    self.offTime.snp.makeConstraints { make in
      make.top.equalTo(self.workTime.snp.bottom).offset(15)
      make.leading.equalTo(30)
      make.centerY.equalTo(offTimeDisplay)
    }
    self.punchTime.snp.makeConstraints { make in
      make.top.equalTo(self.offTime.snp.bottom).offset(30)
      make.leading.equalTo(30)
    }

    self.workTimeDisplay.snp.makeConstraints { make in
      make.top.equalTo(self.displayTimeBtn.snp.bottom).offset(15)
      make.trailing.equalTo(-30)
    }
    self.offTimeDisplay.snp.makeConstraints { make in
      make.top.equalTo(self.workTime.snp.bottom).offset(15)
      make.height.equalTo(60)
      make.trailing.equalTo(-30)
    }
    self.punchTimeOnDisplay.snp.makeConstraints { make in
      make.top.equalTo(self.offTimeDisplay.snp.bottom).offset(15)
      make.trailing.equalTo(-30)
    }
    self.punchTimeOffDisplay.snp.makeConstraints { make in
      make.top.equalTo(self.punchTimeOnDisplay.snp.bottom).offset(15)
      make.trailing.equalTo(-30)
    }

  }
}
