//
// Created by Jaxun on 2018/6/17.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class PunchTimeDetailList: UIViewController {

  fileprivate var info = [""] // todo: 外面帶進來
  fileprivate let tableView = UITableView(frame: .zero, style: .plain)

  required override init?(coder aDecoder:NSCoder) {
    super.init(coder: aDecoder)
  }

  required override init(nibName nibNameOrNil:String?, bundle nibBundleOrNil:Bundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorStyle = .none

    self.view.addSubview(tableView)

    tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    tableView.snp.makeConstraints { make in
      make.edges.equalToSuperview()
    }
  }

  func updateInfo(_ allPunchTimes:[String]) {
    self.info = allPunchTimes
    tableView.reloadData()
  }
}

private typealias TableDelegate = PunchTimeDetailList

extension TableDelegate: UITableViewDelegate {

}

private typealias TableDataSource = PunchTimeDetailList

extension TableDataSource: UITableViewDataSource {
  public func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
    return info.count
  }

  public func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
    if let timeLabel = cell.textLabel {
      timeLabel.text = "\(info[indexPath.row])"
      timeLabel.textAlignment = .center
    }
    return cell
  }
}
