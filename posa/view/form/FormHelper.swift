//
// Created by Jaxun on 2018/8/9.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import UIKit

struct FormHelper {
  static let bpmId = Pref().base64AccountId
  static let bpmTel = Pref().base64BPMTel
  static let bpmCode = Pref().uniqueBPMCode
  static let bpmCode2 = Pref().uniqueBPMCode2

  static func bpmPath(_ name:String) -> String {
    return "\(BuildConfig.bpmEndpoint)FM7_BPMPlus_\(name).aspx?" + // FM7_BPMPlus_CheckAction2.aspx?"
        "AccountID=\(bpmId!)&" +
        "IMEI=\(bpmCode!)&" +
        "CellNo=\(bpmTel!)&" +
        "DeviceToken=\(bpmCode2!)&" +
        "Act=0&" +
        "Lang=en-us"
  }
}
