//
// Created by STARLUX on 1/4/17.
// Copyright (c) 2017 STARLUX. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit
import Kingfisher


public struct ResizingCropCenterImageProcessor: ImageProcessor {
  public let identifier:String

  /// Target size of output image should be.
  public let targetSize:CGSize

  /// Initialize a `ResizingImageProcessor`
  ///
  /// - parameter targetSize: Target size of output image should be.
  ///
  /// - returns: An initialized `ResizingImageProcessor`.
  public init(targetSize:CGSize) {
    self.targetSize = targetSize
    self.identifier = "com.STARLUX.viva.ResizingCropCenterImageProcessor(\(targetSize))"
  }

  public func process(item:ImageProcessItem, options:KingfisherOptionsInfo) -> Image? {
    switch item {
    case .image(let image):
      let uiimage:UIImage = image as UIKit.UIImage
      var keepRatioSize:CGSize = CGSize(width: 0, height: 0)

      if (self.targetSize.width > self.targetSize.height) {
        keepRatioSize = CGSize(width: self.targetSize.width, height: uiimage.size.height * (self.targetSize.width / uiimage.size.width))
      } else {
        keepRatioSize = CGSize(width: uiimage.size.width * (self.targetSize.height / uiimage.size.height), height: self.targetSize.height)
      }

      return self.crop(uiimage.kf.resize(to: keepRatioSize), cropTo: targetSize)
    case .data(_):
        return (DefaultImageProcessor.default >> self).process(item: item, options: options)
    }
  }

  private func crop(_ image:UIImage, cropTo cropSize:CGSize) -> UIImage? {
    let refWidth:CGFloat = image.size.width
    let refHeight:CGFloat = image.size.height

    let x:CGFloat = (refWidth - cropSize.width) / 2.0
    let y:CGFloat = (refHeight - cropSize.height) / 2.0

    let cropRect:CGRect = CGRect(x: x, y: y, width:cropSize.width, height:cropSize.height)
    let imageRef = image.cgImage!.cropping(to: cropRect)
    return UIImage(cgImage: imageRef!)
  }
}
