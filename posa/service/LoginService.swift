//
// Created by Jaxun on 2018/5/21.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift

class LoginService: BaseService {

  func fetchAlias(account:String, password:String) -> Observable<Any> {
    let body = [
      "account": account, "password": password]
    return jsonPost("login", body: body)
  }

}
