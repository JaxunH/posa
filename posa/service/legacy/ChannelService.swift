//
// Created by jaxun on 12/27/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire

class ChannelService: BaseService {

  func getChannels(programId:Int?, channelId:Int?) -> Observable<Array<ChannelDto>> {
    return jsonGet("channels", params: ["programId": programId, "channelId": channelId], resourceNotFoundDefault: [])
        .map(dtoArrayMapper(ChannelDto.fromDictionary))
  }

  func getHotPrograms(channelId:Int, pageNumber:Int, size:Int, categoryId:Int64?) -> Observable<Array<ProgramHotDto>> {
    return jsonGet("channels/\(channelId)/programs/hot",
        params: ["index": pageNumber, "size": size, "categoryId": categoryId],
        resourceNotFoundDefault: [:])
        .map(pagerMapper("programs", ProgramHotDto.fromDictionary))
  }

  func getHotVideos(channelId:Int, pageNumber:Int, size:Int, categoryId:Int64?) -> Observable<Array<VideoHotDto>> {
    return jsonGet("channels/\(channelId)/videos/hot",
        params: ["index": pageNumber, "size": size, "categoryId": categoryId], resourceNotFoundDefault: [:])
        .map(pagerMapper("videos", VideoHotDto.fromDictionary))
  }

  func getLatestVideosByChanneldId(channelId:Int,
      pageNumber:Int,
      size:Int,
      categoryId:Int64?) -> Observable<Array<VideoHotDto>> {
    return jsonGet("channels/\(channelId)/videos/latest",
        params: ["index": pageNumber, "size": size, "categoryId": categoryId], resourceNotFoundDefault: [:])
        .map(pagerMapper("videos", VideoHotDto.fromDictionary))
  }

}
