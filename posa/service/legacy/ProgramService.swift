//
// Created by jaxun on 12/27/16.
// Copyright (c) 2018 STARLUX. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire

class ProgramService: BaseService {

  func getProgramSubscriptionCount(_ programId:Int) -> Observable<Int> {
    return jsonGet("programs/\(programId)/subscriptions/count")
        .map {
          $0 as! Int
        }
  }

  func getProgram(_ programId:Int) -> Observable<ProgramDto> {
    return jsonGet("programs/\(programId)")
        .map(dtoMapper(ProgramDto.fromDictionary))
  }

  func getVideosCountByProgramAndVideoType(programId:Int, videoTypeId:Int) -> Observable<Int> {
    return jsonGet("programs/\(programId)/videos/count", params: ["videoTypeId": videoTypeId])
        .map {
          ($0 as! Dictionary<String, Int>)["size"]!
        }
  }

  func getVideosByProgramAndVideoType(programId:Int,
      pageNumber:Int,
      size:Int,
      videoTypeId:Int) -> Observable<Array<VideoProgramDto>> {
    let params = ["index": pageNumber, "size": size, "videoTypeId": videoTypeId]
    return jsonGet("programs/\(programId)/videos", params: params, resourceNotFoundDefault: [:])
        .map(pagerMapper("videos", VideoProgramDto.fromDictionary))
  }
}
